const express = require('express');
const redis = require("redis");
const JWTRedisSession = require("jwt-redis-session");
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const seedDatabaseIfNeeded = require('./configuration/seed');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/APIAuth');
const JWT_SECRET = require('./configuration');
const redisClient = redis.createClient(),
    secret = "qwerty",
    app = express();

app.use(cors());

app.use(JWTRedisSession({
    client: redisClient,
    secret: secret,
    keyspace: "sess:",
    maxAge: 86400,
    algorithm: "HS256",
    requestKey: "jwtSession",
    requestArg: "jwtToken"
}));

const port = process.env.PORT || 4040;

app.use(morgan('dev'));
app.use(bodyParser.json());

app.use('/users', require('./routes/users'));
app.use('/products', require('./routes/products'));
app.listen(port);
console.log('server listening at', port);


seedDatabaseIfNeeded();
