const express = require('express');
const router = require('express-promise-router')();

const {validateBody, schemas} = require('../helpers/routeHelpers');
const ProductsController = require('../controllers/products');

router.get('/', ProductsController.show);


module.exports = router;