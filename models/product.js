const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    title: String,
    description: String,
    price: Number,
    rating: Number,
    picture: String,
    tags: [],
    active: Boolean
});

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
