require('babel-register');
const JWT = require('jsonwebtoken');
const session = require('express-session');
const JWTRedisSession = require("jwt-redis-session");
const {JWT_SECRET} = require('../configuration');
// const app = require('../app');
const User = require('../models/user');

signToken = user => {
    return JWT.sign({
        iss: 'codeworker',
        sub: user.id,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + 1)
    }, JWT_SECRET);
};

module.exports = {


    signUp: async (req, res, next) => {
        const {email, password} = req.body;
        const foundUser = await User.findOne({email});
        if (foundUser) {
            console.log('user allready exist');
            return res.status(403).json({error: 'user allready exist'})
        }
        const newUser = new User({email, password});

        console.log('users controller sign Up', req.body, foundUser);
        await  newUser.save();

        const token = signToken(newUser);
        res.status(200).json({token});
    },
    signIn: async (req, res, next) => {
        const token = signToken(req.user);
        console.log('users controller sign In', req.user.email);

        req.jwtSession.user = req.user.toJSON();

        const claims = {
            iss: "my application name",
            aud: "myapplication.com"
        };

        req.jwtSession.create(claims, function (error, token) {
            res.send({token: token});
        });
    },
    checkToken: (req, res) => {

    },
    logOut: (req, res) =>{
        req.jwtSession.destroy(function(err) {
            if(err) {
                console.log(err);
            } else {
                res.redirect('/');
            }
        });

    },
    secret: async (req, res, next) => {
        console.log('users controller Secret');
        res.json({secret: 'resource'})
    }
};
