const Product = require('../models/product');
Product.find({}).remove()
    .then(() => {
            Product.create(
                {
                    title: 'Apple iPhone 6 Plus',
                    description: 'Apple iPhone 6 Plus - 64GB (Factory Unlocked) Smartphone - Gray Silver Gold',
                    brand: 'Apple',
                    price: 97.46,
                    picture: 'https://i.ebayimg.com/thumbs/images/m/mvViSCj6gnAq2BDDHsXKS0w/s-l225.jpg',
                    rating: 4.8,
                    active: true
                }, {
                    title: 'Apple iPhone 6S',
                    description: 'New Apple iPhone 6S 16GB GSM Unlocked smarthone Rose Gold Silver Space Gray',
                    brand: 'Apple',
                    price: 123.49,
                    picture: 'https://i.ebayimg.com/thumbs/images/g/iWEAAOSwYBZZ4klN/s-l225.jpg',
                    rating: 4.3,
                    active: true
                }, {
                    title: 'Samsung Galaxy Note 8',
                    description: 'Samsung Galaxy Note 8 N9500 Dual SIM Black 256GB 6GB 6.3" Phone USA FREESHIP*',
                    brand: 'Samsung',
                    price: 1087.59,
                    picture: 'https://i.ebayimg.com/thumbs/images/g/yjMAAOSwbURZsRVT/s-l225.jpg',
                    rating: 3.9,
                    active: true
                }, {
                    title: 'Xiaomi Mi Mix 2',
                    description: 'Xiaomi Mi Mix 2 Smartphone Android 7.1 Snapdragon 835 Octa Core GPS NFC Touch ID',
                    brand: 'Xiaomi',
                    price: 113.99,
                    picture: 'https://i.ebayimg.com/thumbs/images/m/meK4Y4rjgE-0m9YGfrOvVBQ/s-l225.jpg',
                    rating: 4.9,
                    active: true
                }, {
                    title: '5.0\'\' Sony Ericssion Xperia Z',
                    description: '5.0" Sony Ericssion Xperia Z C6603 4G LTE GPS Unlocked 16GB Cell Phone 3 colors',
                    brand: 'Sony',
                    price: 150,
                    picture: 'https://i.ebayimg.com/thumbs/images/m/mB7_SFMhL034Mp7s4MU-7_Q/s-l225.jpg',
                    rating: 4.9,
                    active: true
                }, {
                    title: 'Meizu Pro 7',
                    description: 'Meizu Pro 7 Smartphone Android 7.0 Helio P25 Octa Core 5.2 Inch Screen WIFI GPS',
                    price: 322.99,
                    rating: 4.7,
                    picture: 'https://i.ebayimg.com/thumbs/images/m/my1VklAmwF-PzW81KivZZcA/s-l225.jpg',
                    active: true
                }
            )
        }
    )
    .catch(err => console.log('error populating porducts', err));

module.exports = Product;
